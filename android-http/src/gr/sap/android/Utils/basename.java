package gr.sap.android.Utils;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

public class basename {
	private static final Pattern BASENAME = Pattern.compile(".*?([^/]*)$");
	//private static final Pattern BASENAME = Pattern.compile("/(.*?)$");
	 
	public static String basename(URL url) {
		return basename(url.getPath());
	}
 
	public static String basename(final String url) {
		Matcher matcher = BASENAME.matcher(url);
		if (matcher.matches()) {
			String base =  matcher.group(1);
			return base;
		} else {
			throw new IllegalArgumentException("Can't parse " + url);
		}
	}
}
