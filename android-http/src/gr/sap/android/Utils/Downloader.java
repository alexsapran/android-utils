package gr.sap.android.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateUtils;
import android.util.Log;

public class Downloader {
	private final String TAG = "Downloader";
	private final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    private final String ENCODING_GZIP = "gzip";
    private final int SECOND_IN_MILLIS = (int) DateUtils.SECOND_IN_MILLIS;
    private final String USER_AGENT = "Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
    private boolean mVerbose = false;
    private String HttpEncoding = HTTP.UTF_8;  
    private int Timeout = 60;
    private InputStream mis = null;
	
    /**
     * General Constructor
     */
	public Downloader(){
		
	}
	/**
	 * Constractor with request timeout in sec and encoding
	 * @param int Timeout default 20 sec
	 * @param HTTP Encoding string default HTTP.UTF_8
	 */
	public Downloader(int Timeout,String Encoding){
		this.Timeout = Timeout;
		this.HttpEncoding = Encoding;
	}
	/**
	 * Constractor with encoding
	 * @param HTTP Encoding string default HTTP.UTF_8
	 */
	public Downloader(String Encoding){
		this.HttpEncoding = Encoding;
	}
	
	public InputStream Get(String url) throws Exception{
		return this.Get(url,null);
	}
	
	public InputStream retrieveStream(String url) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpRequest = new HttpGet(url);
        try {
           HttpResponse httpResponse = client.execute(httpRequest);
           final int statusCode = httpResponse.getStatusLine().getStatusCode();
           if (statusCode != HttpStatus.SC_OK) {
              Log.w(TAG,"Error => " + statusCode + " => for URL " + url);
              return null;
           }
           HttpEntity httpEntity = httpResponse.getEntity();
           return httpEntity.getContent();
        }
        catch (IOException e) {
        	httpRequest.abort();
        	Log.w(TAG, "Error for URL =>" + url, e);
        }
        return null;

    }
	public InputStream Get(String url,HashMap<String,String> params) throws Exception{
		StringBuilder uriBuilder = new StringBuilder(url);
		if (params!=null){
			int i = 0;
	    	for (Entry<String, String> entry: params.entrySet()){ 
	    		if (i==0)
	    			uriBuilder.append("?"+entry.getKey()+"=" + entry.getValue());
	    		else
	    			uriBuilder.append("&"+entry.getKey()+"=" + entry.getValue());
	    		i++;
	    	}
		}
		Log.i(TAG,"Request to:"+uriBuilder.toString());
		final HttpClient client = getHttpClient();
		final HttpGet request = new HttpGet(uriBuilder.toString());
        
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, Timeout*1000);
        HttpConnectionParams.setSoTimeout(httpParameters, Timeout*1000);
        request.setParams(httpParameters);

        try {
            HttpResponse response = client.execute(request);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) { 
                if (mVerbose) Log.w(TAG, "Error " + statusCode + " while retrieving data from " + url); 
                throw new HttpException("Error while reteaving url with status:"+statusCode+" from "+url);
            }
            
            final HttpEntity entity = response.getEntity();
            if (entity != null) {
            	mis = new FlushedInputStream(entity.getContent()); 
            	return mis;
            }else{
            	if (mVerbose)Log.e(TAG,"Error with entity");
	        	throw new Exception("Empty http entity returned.");
            }
        } catch (Exception e) {
        	request.abort();
        	if (mVerbose)Log.w(TAG, "Error while retrieving " + url +" "+ e.toString());
        	throw new Exception(e);
        }
	}

	
	/*
	 * Takes a URL with the appropriate parameters and returns a JSON object.
	 * This method assumes that the host will return a JSONObject URL responce.
	 * @Param url: The URL of the host.
	 * @Param params: The query parameters for the URL.
	 */
	public JSONObject GetJSONObject(String url,HashMap<String,String> params) throws Exception{
		StringBuilder stringBuffer = new StringBuilder();
		InputStreamReader in = new InputStreamReader( /*retrieveStream(url));*/this.Get(url, params) );
    	BufferedReader buff = new BufferedReader(in);
    	String line;
	    while ((line = buff.readLine()) != null) {
	    	stringBuffer.append(line);
        }
    	return new JSONObject(stringBuffer.toString());	
		
	}
	
	/*
	 * Takes a URL with the appropriate parameters and returns a JSON array.
	 * This method assumes that the host will return a JSONArray URL responce.
	 * @Param url: The URL of the host.
	 * @Param params: The query parameters for the URL.
	 */
	public JSONArray GetJSONArray(String url,HashMap<String,String> params) throws Exception{
		StringBuilder stringBuffer = new StringBuilder();
		InputStreamReader in = new InputStreamReader( retrieveStream(url) );
    	BufferedReader buff = new BufferedReader(in);
    	String line;
	    while ((line = buff.readLine()) != null) {
	    	stringBuffer.append(line);
        }
    	return new JSONArray(stringBuffer.toString());	
		
	}
	
	public InputStream Post(String url) throws Exception{
		return this.Post(url,null);
	}
	
	public InputStream Post(String url,HashMap<String,String> params) throws Exception{
		
		HttpClient client = new DefaultHttpClient();  
        HttpPost post = new HttpPost(url); 
        List<NameValuePair> param = new ArrayList<NameValuePair>();
        for (Entry<String, String> entry: params.entrySet()) 
        	param.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        
        try {
	        UrlEncodedFormEntity ent = new UrlEncodedFormEntity(param,HttpEncoding);
	        post.setEntity(ent);
        
	        HttpResponse response = client.execute(post);  
	        final int statusCode = response.getStatusLine().getStatusCode();
	        if (statusCode != HttpStatus.SC_OK) { 
	        	if (mVerbose) Log.w(TAG, "Error " + statusCode + " while retrieving data from " + url); 
	            throw new HttpException("Error while reteaving url with status:"+statusCode+" from "+url);
	        }
	        HttpEntity entity = response.getEntity();
	        if (entity != null) {
	        	mis = new FlushedInputStream(entity.getContent()); 
            	return mis;
	        }else{
	        	if (mVerbose)Log.e(TAG,"Error with entity");
	        	throw new Exception("Empty http entity returned.");
	        }
        }catch (Exception e) {
        	post.abort();
            Log.w(TAG, "Error while retrieving " + url +" "+ e.toString());
            throw new Exception(e);
        }
	}
	
	/**
	 * Executes a HTTP Head request in order to validate live http connectivity.
	 * 
	 * @param String url
	 * @return boolean if you can do HTTP
	 * @throws ClientProtocolException
	 */
	public boolean HEAD(String url) throws ClientProtocolException{
		HttpHead head = new HttpHead(url);
		HttpClient client = new DefaultHttpClient(); 
		HttpResponse response;
		try {
			response = client.execute(head);
		} catch (IOException e) {
			if (mVerbose)Log.e(TAG,e.getLocalizedMessage());
			return false;
		} 
		final int statusCode = response.getStatusLine().getStatusCode();
		return (statusCode==HttpStatus.SC_OK);
	}
	
	public String ClearJSONP(String res){
		if (res==null) return null;
		String ret = "";
		String pat = "\\((.*?)\\);";
		Pattern p = Pattern.compile(pat,Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(res);
		if (m.find()){
			String tmp = m.group();
			tmp = tmp.substring(1, tmp.length()-1);
			ret = tmp;
		}else{
			ret = res;
		}
		return ret;
	}
	/**
     * Generate and return a {@link HttpClient} configured for general use,
     * including setting an application-specific user-agent string.
     */
    public HttpClient getHttpClient() {
        final HttpParams params = new BasicHttpParams();

        // Use generous timeouts for slow mobile networks
        HttpConnectionParams.setConnectionTimeout(params, Timeout * SECOND_IN_MILLIS);
        HttpConnectionParams.setSoTimeout(params, Timeout * SECOND_IN_MILLIS);

        HttpConnectionParams.setSocketBufferSize(params, 8192);
//        HttpProtocolParams.setUserAgent(params, buildUserAgent(context));
        HttpProtocolParams.setUserAgent(params, USER_AGENT);

        final DefaultHttpClient client = new DefaultHttpClient(params);

        client.addRequestInterceptor(new HttpRequestInterceptor() {
        	public void process(HttpRequest request, HttpContext context) {
                // Add header to accept gzip content
                if (!request.containsHeader(HEADER_ACCEPT_ENCODING)) {
                    request.addHeader(HEADER_ACCEPT_ENCODING, ENCODING_GZIP);
                }
            }
        });

        client.addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse response, HttpContext context) {
                // Inflate any responses compressed with gzip
                final HttpEntity entity = response.getEntity();
                final Header encoding = entity.getContentEncoding();
//                if (encoding != null) {
//                    for (HeaderElement element : encoding.getElements()) {
//                        if (element.getName().equalsIgnoreCase(ENCODING_GZIP)) {
//                        	response.setEntity((HttpEntity) new GzipDecompressingEntity(response.getEntity())); 
//                            break;
//                        }
//                    }
//                }
                try {
	                if (isCompressed(toByte(entity.getContent())) ) {
	                	response.setEntity((HttpEntity) new GzipDecompressingEntity(response.getEntity())); 
	                }
                }catch (IOException e) {
				}
            }
        });

        return client;
    }
    
    public byte[] toByte(InputStream is) throws IOException {
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	int next = is.read();
    	while (next > -1) {
    	    bos.write(next);
    	    next = is.read();
    	}
    	bos.flush();
    	return bos.toByteArray();
    }
    /*
     * Determines if a byte array is compressed. The java.util.zip GZip
     * implementaiton does not expose the GZip header so it is difficult to determine
     * if a string is compressed.
     * 
     * @param bytes an array of bytes
     * @return true if the array is compressed or false otherwise
     * @throws java.io.IOException if the byte array couldn't be read
     */
     public boolean isCompressed(byte[] bytes) throws IOException
     {
          if ((bytes == null) || (bytes.length < 2))
          {
               return false;
          }
          else
          {
                return ((bytes[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (bytes[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)));
          }
     }
    static class GzipDecompressingEntity extends HttpEntityWrapper {
        public GzipDecompressingEntity(final HttpEntity entity) {
            super(entity);
        }
    
        @Override
        public InputStream getContent()  throws IOException, IllegalStateException {
            // the wrapped entity's getContent() decides about repeatability
            InputStream wrappedin = wrappedEntity.getContent();
            return new GZIPInputStream(new BufferedInputStream(wrappedin));
        }
        @Override
        public long getContentLength() {
            // length of ungzipped content is not known
            return -1;
        }

    } 
    static class FlushedInputStream extends FilterInputStream {
	    public FlushedInputStream(InputStream inputStream) {
	        super(inputStream);
	    }

	    @Override
	    public long skip(long n) throws IOException {
	        long totalBytesSkipped = 0L;
	        while (totalBytesSkipped < n) {
	            long bytesSkipped = in.skip(n - totalBytesSkipped);
	            if (bytesSkipped == 0L) {
	                  int bbyte = read();
	                  if (bbyte < 0) {
	                      break;  // we reached EOF
	                  } else {
	                      bytesSkipped = 1; // we read one byte
	                  }
	           }
	            totalBytesSkipped += bytesSkipped;
	        }
	        return totalBytesSkipped;
	    }
	}
    public String toString(){
    	try {
			return Stream2String(mis);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
    }
    public Bitmap toBitMap() {
    	return BitmapFactory.decodeStream(mis);
    }
    public String Stream2String (InputStream i) throws IOException {
    	StringBuilder stringBuffer = new StringBuilder();
		InputStreamReader in = new InputStreamReader( i );
    	BufferedReader buff = new BufferedReader(in);
    	String line;
	    while ((line = buff.readLine()) != null) {
	    	stringBuffer.append(line);
        }
	    return stringBuffer.toString();
    }
    public String Convert(InputStream is) {
        if (is ==null) return "";
    	/*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
